<?php

/************************************************************************
* Page Sections template
*************************************************************************/


?>



	<section style="position:relative;" style="" class="page-section <?php echo get_post_type();?>-<?php the_ID();?> <?php echo (isset($bg)) ? $bg : '';?>" id="<?php echo $post->post_name;?>">
		<div class="container">

<?php if( get_field('background') ): 
$image = get_field('background'); 
echo '<script>
jQuery("script").last().parent().parent().css("background-image","url(';
echo $image['sizes']['large']; 
echo ')");</script>';
endif; ?>
			<?php

			the_content();

			?>

	<?php
// check if the repeater field has rows of data
if( have_rows('attractions') ):
echo '<div class="horz">';
 	// loop through the rows of data
    while ( have_rows('attractions') ) : the_row();
echo '<div style="background-image:url(';
$image = the_sub_field('image');
echo $image;
echo ');" class="location">';
        // display a sub field value
echo '<h4>';
        the_sub_field('title');
echo '</h4><p>';
        the_sub_field('copy');
echo '<a class="maplink" target="_blank" href="';
        the_sub_field('link');
echo '">More Info</a><span class="divider">&nbsp;|&nbsp;</span><a class="maplink" target="_blank" href="https://www.google.com/maps/dir//';

str_replace(" ", "+", the_sub_field('address'));

echo '">Directions</a></p></div>';
    endwhile;
echo '</div>';
else :

    // no rows found

endif;

// check if the repeater field has rows of data
// if( have_rows('local_area') ):
// echo '<div class="horz">';
//  	// loop through the rows of data
//     while ( have_rows('local_area') ) : the_row();
// echo '<div style="background-image:url(';
// $image = the_sub_field('image');
// echo $image;
// echo ');" class="location">';
//         // display a sub field value
// echo '<h4>';
//         the_sub_field('title');
// echo '</h4><p>';
//         the_sub_field('copy');
// echo '<a class="maplink" target="_blank" href="';
//         the_sub_field('link');
// echo '">More Info</a>&nbsp;|&nbsp;<a class="maplink" target="_blank" href="https://www.google.com/maps/dir//';

// str_replace(" ", "+", the_sub_field('address'));

// echo '">Directions</a></p></div>';
//     endwhile;
// echo '</div>';
// else :

    // no rows found

// endif;
?>
<?php 
// if( have_rows('local_area') || have_rows('attractions') ):
// echo '<div id="bx-pager-1"><div> 
// <a data-slide-index="0" class=""><span>Attractions</span></a>
// <a data-slide-index="1" class=""><span>Restaurants</span></a></div></div>';
// else :
// endif;
?>

	<?php

// check if the repeater field has rows of data
if( have_rows('rooms') ):
echo '<ul class="bxslider-2">';
	$i = 1;
 	// loop through the rows of data
    while ( have_rows('rooms') ) : the_row();
    echo '<li>';
		if( have_rows('rooms_images') ):
	echo '<script type="text/javascript">jQuery(document).ready(function ($) {$(window).load(function() {$("#rooms-section .container .horzsContainer > li:nth-child(';
	echo $i;
	echo ')").backstretch([';
			while ( have_rows('rooms_images') ) : the_row();
					echo '"';
					$image = get_sub_field('rooms_image');
					$size = 'large';
					echo $image['sizes'][$size];
					echo '",';
			endwhile;
			echo '], {duration: 4000, fade: "normal"});});});</script>';
		endif;
        // display a sub field value
	echo '<div class="eight columns alpha">';
       the_sub_field('rooms_content');
	echo '</div>';
	echo '<a class="book-now right-now eight columns omega" href="https://bookings.ihotelier.com/Marinwood-Inn-%26-Suites/bookings.jsp?hotelId=96280&amp;rateplanid=1586994">Book Now</a>';
	echo '</li>';
	$i++;
    endwhile;
echo '</ul>';
echo '<div class="bottom-navigation"><div>';
echo '<div class="room-name"></div>';
	$i = 0;
	while ( have_rows('rooms') ) : the_row();
		if( have_rows('rooms_images') ):
			while ( have_rows('rooms_images') ) : the_row();
					echo '<a href="#rooms/';
					echo $i;
					echo '"><img src="';
					$image = get_sub_field('rooms_image');
					$size = 'thumbnail';
					echo $image['sizes'][$size];
					echo '"></a>';
					
					break;
			endwhile;
		endif;	
		$i++;
	endwhile;		

echo '</div></div>';

else :

    // no rows found

endif;

?>

	<?php

// check if the repeater field has rows of data
if( have_rows('specials') ):
echo '<ul class="bxslider-3">';
	$i = 1;
 	// loop through the rows of data
    while ( have_rows('specials') ) : the_row();
    echo '<li>';
		if( have_rows('specials_images') ):
	echo '<script type="text/javascript">jQuery(document).ready(function ($) {$(window).load(function() {$("#specials-section .container .horzsContainer > li:nth-child(';
	echo $i;
	echo ')").backstretch([';
			while ( have_rows('specials_images') ) : the_row();
					echo '"';
					$image = get_sub_field('specials_image');
					$size = 'large';
					echo $image['sizes'][$size];
					echo '",';
			endwhile;
			echo '], {duration: 4000, fade: "normal"});});});</script>';
		endif;
        // display a sub field value
	echo '<div class="eight columns alpha">';
       the_sub_field('specials_content');
	echo '</div>';
	echo '<a class="book-now right-now eight columns omega" href="https://bookings.ihotelier.com/Marinwood-Inn-%26-Suites/bookings.jsp?hotelId=96280&amp;rateplanid=1586994">Book Now</a>';
	echo '</li>';
	$i++;
    endwhile;
echo '</ul>';
echo '<div class="bottom-navigation"><div>';
echo '<div class="special-name"></div>';
	$i = 0;
	while ( have_rows('specials') ) : the_row();
		if( have_rows('specials_images') ):
			while ( have_rows('specials_images') ) : the_row();
					echo '<a href="#specials/';
					echo $i;
					echo '"><img src="';
					$image = get_sub_field('specials_image');
					$size = 'thumbnail';
					echo $image['sizes'][$size];
					echo '"></a>';
					
					break;
			endwhile;
		endif;	
		$i++;
	endwhile;		

echo '</div></div>';

else :

    // no rows found

endif;

?>



		</div> <!-- ./container -->
	</section> <!-- ./services -->