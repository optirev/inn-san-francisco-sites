<?php

/************************************************************************
* Page sections for page/post
*************************************************************************/

if (!empty($_SERVER['SCRIPT_FILENAME']) && 'page-sections.php' == basename($_SERVER['SCRIPT_FILENAME'])){

	die ('This file can not be accessed directly!');
}

?>

<section class="page-section alternate-bg1 <?php echo get_post_type();?>-<?php echo get_the_ID();?>" id="<?php echo $post->post_name;?>">
	<div class="container">
		<?php

		the_content();

		?>

<?php

// check if the repeater field has rows of data
if( have_rows('local_area') ):

 	// loop through the rows of data
    while ( have_rows('local_area') ) : the_row();
echo '<div style="background-image:url(';
echo the_sub_field('image');
echo ');" class="location">';
        // display a sub field value
echo '<h4>';
        the_sub_field('title');
echo '</h4>';
        the_sub_field('copy');
echo '</div>';
    endwhile;

else :

    // no rows found

endif;

?>
	</div> <!-- ./container -->
</section> <!-- ./services -->
