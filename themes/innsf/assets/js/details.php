<?php 
function file_get_contents_curl($url) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);       

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

    header('Content-type: application/json');
    echo file_get_contents_curl("https://maps.googleapis.com/maps/api/place/details/json?" .
      "reference=" . $_GET['reference'] .
      "&sensor=" . $_GET['sensor'] .
      "&key=" .$_GET['key']);
?>