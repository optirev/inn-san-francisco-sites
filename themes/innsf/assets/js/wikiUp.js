;(function(global, undefined) {
    "use strict";

    if (!global.jQuery) {
        return false;
    }

    var $ = global.jQuery,
        wikiQuantity = 0,

        DEFAULTS = {
            error: 'Error. Try again.',
            loading: 'Loading',
            lang: 'en'
        };

    var wikiUp = function($element, options) {
            var containerId = 'wiki-' + (wikiQuantity++);
            $element
                .data('wikiUp', containerId)
                .bind('mouseover', function() {
                    if (!$element.children('.tooltip').length) {
                        $element.append('<div class="tooltip"><span></span><div id="' + containerId + '"></div></div>');
                        /*
                    if (!$element.children('.wikiContent').length) {
                        $element.append('<div class="wikiContent"><div class="wikiInner" id="' + containerId + '"></div></div>');*/
                        wikiLoad($element, options);
                    }
                });
        };

                    var location = 0;
                    var reference = 0;
                    var address = 0;
                    var photoReference = 0;
                    var photoUrl = 0;
                    var website = "#";
                    var $allText = false;

    var wikiLoad = function($element, options) {
            var $container = $('#' + $element.data('wikiUp')),
                lang = $element.data('lang') || options.lang,
                page = $element.data('wiki').split(' ').join(' '),
                url = 'http://' + lang + '.wikipedia.org/w/api.php?callback=?&action=parse&page=' + page + '&prop=text&format=json&section=0';

            $.ajax({
                type: 'GET',
                url: url,
                data: {},
                async: true,
                contentType: 'application/json; charset=utf-8',
                dataType: 'jsonp',
                success: function(response) {
                    var found = false,
                        paragraphCount = 0,
                        intro;
                        if (response.parse) {
                            $allText = $(response.parse.text['*']);
                        }
                        else {
                            $allText = false;
                        }
                    while (found === false) {
                        found = true;
                            if ($allText!=false){
                                if ($allText.filter('p:eq(' + paragraphCount + ')').html()){
                                intro = $allText.filter('p:eq(' + paragraphCount + ')').html();
                                }
                                else {
                                intro = "";
                                }
                            }
                            else {
                                intro = "";
                            }
                        if (intro.indexOf('<span') === 0) {
                            paragraphCount++;
                            found = false;
                        }
                    }


                    location = page.split(' ').join('+').replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/g, "");

                    $.ajax({
                      url: 'places.php',
                      dataType: "json",
                      async: false,
                      data: {
                        query: location,
                        sensor: false,
                        key: 'AIzaSyDGoK_vG6c2iK0o5n5HG8193YtJai4lLTA'
                      },
                      type: "GET", //or POST if you want => update your php in that case
                      success: function(data) {
                        if (data.results[0]){
                            reference = data.results[0].reference;

                        $.ajax({
                          url: 'details.php',
                          dataType: "json",
                          async: false,
                          data: {
                            reference: reference,
                            sensor: false,
                            key: 'AIzaSyDGoK_vG6c2iK0o5n5HG8193YtJai4lLTA'
                          },
                          type: "GET", //or POST if you want => update your php in that case
                          success: function(data) {
                            if(data.result!=undefined){
                            address = data.result.adr_address;
                            if(data.result.website) {
                              website = data.result.website;
                            } else {
                              website = data.result.url;
                            }
                            }   

                            if (data.result.photos) {
                            photoReference = data.result.photos[0].photo_reference;
                            $.ajax({
                            url: 'photos.php',
                            dataType: "html",
                            async: false,
                            data: {
                              maxwidth: 200,
                              maxheight: 100,
                              photoreference: photoReference,
                              sensor: false,
                              key: 'AIzaSyDGoK_vG6c2iK0o5n5HG8193YtJai4lLTA'
                            },
                            type: "GET", //or POST if you want => update your php in that case
                            success: function(data) {
                              photoUrl = $(data).filter("a").attr('href');
                              photoUrl = '<img src="'+photoUrl+'"/><br>'

                            },
                            error: function(request, status, error) {
                              $('#gpoutput').append(error);
                            }
                          });

                            } 
                            else {
                            photoReference = 0;
                            photoUrl = ""
                            }


                          },
                          error: function(request, status, error) {}
                        });

                        }
                        else {
                            reference = 0;
                        }
                            },
                      error: function(request, status, error) {

                      }
                            });
                    




                    $container
                        .html(photoUrl+'<a href="'+website+'">More Info</a><br>'+address+'<br><br>'+intro)
                        .find('a')
                            .attr('target', '_blank')
                            .not('.references a')
                                .attr('href', function(i, href) {
                                    if (href.indexOf('http') !== 0) {
                                        href = 'http://' + lang + '.wikipedia.org' + href;
                                    }
                                    return href;
                                })
                                .end()
                            .end()
                        .find('sup.reference')
                            .remove();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $container.html(options.error);
                },
                beforeSend: function(XMLHttpRequest) {
                    $container.html(options.loading);
                }
            });
        };

    $.fn.wikiUp = function(options) {
        options = $.extend(true, {}, DEFAULTS, options);

        return this.each(function() {
            var $element = $(this);
            if (!$element.data('wikiUp')) {
                wikiUp($element, options);
            }
        });
    };

    // On document ready
    $(function() {
        // Default selector to parse
        $('[data-wiki]').wikiUp();
    });

}(this));