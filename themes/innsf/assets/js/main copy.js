

jQuery(document).ready(function ($) {

if ($(window).height()<850) {
            $('#hotels .head-image').hide();
        }
        else {
            $('#hotels .head-image').show();
        }


// Change URL



// Hotel Select

$('#hotels-select').on('change', function() {
  if (this.value =='Novato') {
    $('#hotel-site a').attr('href', 'http://www.econolodge.com/hotel-novato-california-CAB35');
    $('#check-site a').attr('href', 'http://www.econolodge.com/ires/en-US/html/RoomAvailability?hotel=CAB35');
}
else if (this.value=="Capri") {
    $('#hotel-site a').attr('href', 'http://www.sfmotelcapri.com/');
    $('#check-site a').attr('href', 'http://www.sfmotelcapri.com/index.php/reservations');
    }
else if (this.value=="Geary") {
    $('#hotel-site a').attr('href', 'http://gearyparkwaymotel.com/');
    $('#check-site a').attr('href', 'http://www.gearyparkwaymotel.com/index.php/reservations');
}
else if (this.value=="Berkeley") {
    $('#hotel-site a').attr('href', 'http://berkeleyri.com/');
    $('#check-site a').attr('href', 'http://www.rodewayinn.com/hotel-berkeley-california-CA996');
}
else {
    
}
});

// Newsletter

if (window.location.hash=="#newsletter") {
    $("#constant_contact_api_widget-2").toggleClass("activesuccess");
    $('.constant-contact-signup').css('overflow','visible');
    setTimeout(function(){
        $("#constant_contact_api_widget-2").toggleClass("activesuccess");
        $('.constant-contact-signup').css('overflow','hidden');
    },10000);

}


$(".newsletter a").bind("click", function() {
    $("#constant_contact_api_widget-2").toggleClass("active");

    if ($("#constant_contact_api_widget-2").hasClass("active")) {
    setTimeout(function(){$('.constant-contact-signup').css('overflow','visible')},500);
    }

    else {
    $('.constant-contact-signup').css('overflow','hidden');
}
});

// Window-sizer based on resolution
enquire.register("screen and (min-width:767px)", {

    match : function() {$('section.page-section').scrollSections(
    {
    speed: 1000,
     touch: true
    });

    var h = $(window).height();
    $('section.page-section').css('height',(h));

    $(window).resize(function(){
        var h = $(window).height();
        var w = $(window).width();
        $('section.page-section').css('height',(h));
    });


},      
                                
    unmatch : function() {},    
      
});

// Add social media to menufication

$('#menufication-nav ul').append('<div id="sm-buttons"><a class="gp-menu" href="#"></a><a class="fb-menu" href="https://www.facebook.com/pages/Inn-San-Francisco/1457904334422518"></a></div>');

// Close menufication on menu click

$('#menufication-nav li a').click(function(e){
e.preventDefault();
var href = this.href;
window.location = href;
MENUFICATION_INSTANCE.toggleMenu2();
});

 // $('section.page-section').windows({
 //                     snapping: true,
 //                     snapSpeed: 500,
 //                     snapInterval: 700,
 //                     onScroll: function(scrollPos){
 //                         // scrollPos:Number
 //                     },
 //                     onSnapComplete: function($el){
 //                         // after window ($el) snaps into place
 //                     },
 //                     onWindowEnter: function($el){
 //                         // when new window ($el) enters viewport
 //                     }
 //                 });
            
// Hotel Sliders

    hotelsslider = $('.bxslider-1').bxSlider({
        pagerCustom: '#bx-pager-1',
        controls: false,
        slideMargin: 100,
        randomStart: true,
        touchEnabled: false,
        easing: 'ease',
        preloadImages: 'all',
        infiniteLoop: 'true',
        preventDefaultSwipeY: 'true',
  //       onSlideAfter: function(){
  //   // // do mind-blowing JS stuff here
  //   //     if (hotelsslider.getCurrentSlide() == 0){
  //   //     $('#tooltip').tooltipster('show');
  //   // }
  //   //     else if (hotelsslider.getCurrentSlide() == 1){
  //   //     $('#tooltip2').tooltipster('show');
  //   // }
  //   //     else if (hotelsslider.getCurrentSlide() == 2){
  //   //     $('#tooltip3').tooltipster('show');
  //   // }
  //   //     else if (hotelsslider.getCurrentSlide() == 3){
  //   //     $('#tooltip4').tooltipster('show');
  //   // }

  // },
  // onSlideBefore: function(){
  //   // if (hotelsslider.getCurrentSlide() == 0){
  //   //     $('#tooltip2').tooltipster('hide');
  //   //     $('#tooltip3').tooltipster('hide');
  //   // }
  //   // else if (hotelsslider.getCurrentSlide() == 1){
  //   //     $('#tooltip').tooltipster('hide');
  //   //     $('#tooltip3').tooltipster('hide');
  //   // }
  //   // else if (hotelsslider.getCurrentSlide() == 2){
  //   //     $('#tooltip2').tooltipster('hide');
  //   //     $('#tooltip').tooltipster('hide');
  //   // }
  //   // else if (hotelsslider.getCurrentSlide() == 3){
  //   //     $('#tooltip').tooltipster('hide');
  //   //     $('#tooltip2').tooltipster('hide');
  //   //     $('#tooltip3').tooltipster('hide');        
  //   // }
  
    
  // }

    });

// Cities slider

    citiesslider = $('.bxslider-2').bxSlider({
        pagerCustom: '#bx-pager-2',
        controls: false,
        slideMargin: 100,
        randomStart: true,
        touchEnabled: false,
        easing: 'ease',
        infiniteLoop: 'true',
        preventDefaultSwipeY: 'true'
    });

// Side Slider

    sideslider = $('.bxslider-sidebar').bxSlider({
        controls: false,
        mode: 'vertical',
        slideMargin: 30,
        pager: false,
        easing: 'ease',
        adaptiveHeight: 'false'
    });

// Swipe function for cities slider

enquire.register("screen and (max-width:767px)", {
match : function() {

 $(".bxslider-2").swipe( {
        swipeLeft:function() {
          citiesslider.goToNextSlide();
        },
        swipeRight:function() {
        var currentc = citiesslider.getCurrentSlide();

         if (currentc == 0){
}
else {
    citiesslider.goToPrevSlide();
}
        },
        threshold:100
      });

// swipe function for hotels slider

 $(".bxslider-1").swipe( {
        swipeLeft:function() {
          hotelsslider.goToNextSlide();
        },
        swipeRight:function() {
            var currenth = hotelsslider.getCurrentSlide();
if (currenth == 0){
}
else {
    hotelsslider.goToPrevSlide();
}
        },
        threshold:100
      });
} 
});

// key binding for sliders

Mousetrap.bind('right', function() {
    hotelsslider.goToNextSlide();
    citiesslider.goToNextSlide();
});

Mousetrap.bind('left', function() {
   var currenth = hotelsslider.getCurrentSlide();
   var currentc = citiesslider.getCurrentSlide();
if (currenth == 0){
}
else {
    hotelsslider.goToPrevSlide();
}

if (currentc == 0){
}
else {
    citiesslider.goToPrevSlide();
}

});

// sticky 

    $(".full-header").sticky({
        topSpacing: 0,
        wrapperClassName: 'full-header'
    });

// mouse icon effect

    $('.image-wrapper').hover(function(){

        $(this).find('.mouse-effect').stop().animate({'opacity':'0.6'});
        $(this).find('.extra-links').stop().animate({'top':'50%'});

    },function(){


        $(this).find('.mouse-effect').stop().animate({'opacity':'0'});
        $(this).find('.extra-links').stop().animate({'top':'-50%'});

    });

/* 

    $("#portfolio-column-change a").click(function (event) {
        event.preventDefault();
        var view = $(this).attr("id");
        if (view == "three") {
            $(".holder.quicksand li").removeClass("four columns").addClass("one-third column");
            if ($data) {
                $("ul.holder.quicksand").removeAttr("style");
                $data.find("li").removeClass("four columns").addClass("one-third column")
            }
        } else {
            $(".holder.quicksand li").removeClass("one-third column").addClass("four columns");
            if ($data) {
                $("ul.holder.quicksand").removeAttr("style");
                $data.find("li").removeClass("one-third column").addClass("four columns")
            }
        }
    });*/

    $("a[rel^='prettyPhoto']").prettyPhoto();

    $(".ads img").addClass("img-frame");

    $('.footer .fr a').click(function(){
        $('html,body').animate({ scrollTop: 0}, 'slow');
    });


     // Set menu height
    


    /*$('#main-menu li a').click(function(e){

        e.preventDefault();

            var content = $(this).attr('href');
            var checkURL = content.match(/^#([^\/]+)$/i);
            if(checkURL){

                var goPosition = $(content).offset().top - (menuHeight);

                $('html,body').animate({ scrollTop: goPosition}, 'slow');

            }else{
                window.location = content;
            }

    });*/
$('#sidebar-logo').click(function(){
$('#scrollsections-navigation a:nth-child(1)').click();
});

function matchmenus() {

    // Fix jQuery conflicts
    $.noConflict();

    var menuitems = 0;

    $('#main-menu li').each(function() {
         $(this).click(function(e){
            e.preventDefault();
        menuitems = $(this).index() + 1;
        $('#scrollsections-navigation a:nth-child('+menuitems+')').click();
    });
    });

};

matchmenus();

/*
    $('a.scrollable').click(function(e){

        e.preventDefault();

            var content = $(this).attr('href');
            var checkURL = content.match(/#([^\/]+)$/i);
            if(checkURL[0]){

                var goPosition = $('section'+checkURL[0]).offset().top - (menuHeight);

                $('html,body').animate({ scrollTop: goPosition}, 'slow');

            }

    });*/


    $("#main-menu li").click(function () {
        $("#main-menu li").removeClass("active");
        $(this).addClass("active");
        if ($(this).hasClass("menu-item-122")) {
            sideslider.goToSlide(0);
        }
        else if ($(this).hasClass("menu-item-26")) {
            sideslider.goToSlide(1);
        }
        else if ($(this).hasClass("menu-item-23")) {
            sideslider.goToSlide(2);
        }
        else if ($(this).hasClass("menu-item-24")) {
            sideslider.goToSlide(3);
        }
        else if ($(this).hasClass("menu-item-25")) {
            sideslider.goToSlide(4);
        }
    });

    $(".filter li a").click(function (event) {
        event.preventDefault();
        var test = $(this).parent().attr("class");
        $(".filter li a").removeClass("main-btn").addClass("gray");
        $(this).removeClass("gray").addClass("main-btn");
    });
    
    $("#foot a").click(function () {
        $("#menu li").removeClass("active");
        $("#menu li:first").addClass("active")
    });

    var adjustParallax = function(){

        $('section.parallax').each(function(){

            var sectionParallax = "#"+$(this).attr('id');

            jQuery(sectionParallax).parallax("50%", "0.3");


        });
    }


    var $filterType = $("#filterOptions li.active a").attr("rel");
    var $holder = $("ul.quicksand");
    var $data = $holder.clone();
    $("#filterOptions li a").click(function (e) {
        $("#filterOptions li").removeClass("active");
        var $filterType = $(this).attr("rel");
        $(this).parent().addClass("active");
        if ($filterType == "all") var $filteredData = $data.find("li");
        else var $filteredData = $data.find("li[data-type~=" + $filterType + "]");
        $holder.quicksand($filteredData, {
            duration: 800,
            easing: "easeInOutQuad"
        }, function () {
            $("a[rel^='prettyPhoto']").prettyPhoto();

            adjustParallax();

            $('ul.quicksand').removeAttr('style');
            
             $('.image-wrapper').hover(function(){

                $(this).find('.mouse-effect').stop().animate({'opacity':'0.6'});
                $(this).find('.extra-links').stop().animate({'top':'50%'});

                },function(){


                $(this).find('.mouse-effect').stop().animate({'opacity':'0'});
                $(this).find('.extra-links').stop().animate({'top':'-50%'});

                });
        });
        return false;
    });





    var lastId, topMenu = $("#main-menu"),
    topMenuHeight = topMenu.outerHeight() + 500;
    menuItems = topMenu.find('a');

        scrollItems = menuItems.map(function () {

            content = $(this).attr("href");

            if(content){
                var checkURL = content.match(/^#([^\/]+)$/i);

                if(checkURL){

                    var item = $($(this).attr("href"));
                    if (item.length) return item

                }
            }
        });

        
    $(window).scroll(function () {
        var fromTop = $(this).scrollTop() + topMenuHeight;
        var cur = scrollItems.map(function () {
            if ($(this).offset().top < fromTop) return this
        });
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";
        if (lastId !== id) {
            lastId = id;
           var thing = menuItems.parent().removeClass("active").end().filter("[href=#" + id + "]").parent().addClass("active")
        if (thing.hasClass("menu-item-122")) {
            sideslider.goToSlide(0);
        }
        else if (thing.hasClass("menu-item-26")) {
            sideslider.goToSlide(1);
        }
        else if (thing.hasClass("menu-item-23")) {
            sideslider.goToSlide(2);
        }
        else if (thing.hasClass("menu-item-24")) {
            sideslider.goToSlide(3);
        }
        else if (thing.hasClass("menu-item-25")) {
            sideslider.goToSlide(4);
        }
        }
    });

    
    $("#iso-column-change a").click(function (event) {
        event.preventDefault();
        var view = $(this).attr("id");
        if (view == "three") {
            $("#iso-portfolio li").removeClass("four columns").addClass("one-third column");
            $('#iso-portfolio').isotope('reLayout');
        } else {
            $("#iso-portfolio li").removeClass("one-third column").addClass("four columns");
            $('#iso-portfolio').isotope('reLayout');

        }

        adjustParallax();
    });


    var $container = $('#iso-portfolio');

    $('#iso-filter a').click(function(event){

        var selector = $(this).attr('rel');

        if(selector != "all"){
            selector = '.'+selector;
        }else{
            selector = '*';
        }

        $container.isotope({ filter: selector },function(){
        
            adjustParallax();

        });
         
        return false;
    });

$( "#cities ul li ul li" ).each(function() {
    var itemindex = $(this).index();
    var name = $(this).html();

    if ($(this).is(':has(span)')){
    }
    else {
  $( this ).wrapInner( "<span data-wiki='"+name+"'></span>" );
    }
});
/*
$( ".attractions li" ).click(function() {
    var name = $(this).html();
    console.log($(this).siblings());
});

page = name;
url = 'http://en.wikipedia.org/w/api.php?callback=?&action=parse&page=' + page + '&prop=text&format=json&section=0';

$container = $(this).siblings();

$.ajax({
                type: 'GET',
                url: url,
                data: {},
                async: false,
                contentType: 'application/json; charset=utf-8',
                dataType: 'jsonp',
                beforeSend: function(jqXHR) {},
                success: function(response) {
                    var found = false,
                        paragraphCount = 0,
                        $allText = $(response.parse.text['*']),
                        intro;
                    while (found === false) {
                        found = true;
                        intro = $allText.filter('p:eq(' + paragraphCount + ')').html();

                        if (intro.indexOf('<span') === 0) {
                            paragraphCount++;
                            found = false;
                        }
                    }
                    console.log(intro);
                    $container
                        .html(intro)
                        .find('a')
                            .attr('target', '_blank')
                            .not('.references a')
                                .attr('href', function(i, href) {
                                    if (href.indexOf('http') !== 0) {
                                        href = 'http://en.wikipedia.org' + href;
                                    }
                                    return href;
                                })
                                .end()
                            .end()
                        .find('sup.reference')
                            .remove();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
console.log('failed');
                },
    complete: function(request, textStatus) {

    }
            }).done(function() {
$container.prepend('<h3>'+name+'</h3>');
$container.parent().prettyPhoto();
});;
                    

});




$( ".attractions li" ).click(function (event) {
        event.preventDefault();
    });
*/
});


jQuery(window).load(function () {

     jQuery(".mainSlider").flexslider({
        animation: "slide",
        animationLoop: true,
        directionNav: false,
        controlNav: true
    });


    jQuery('.nzs-isotype').isotope({
        itemSelector : '.nzs-iso-enabled'
     });

    jQuery(window).resize(function(){
        jQuery('.nzs-isotype').isotope('reLayout',function(){
           jQuery('section.parallax').each(function(){

                var sectionParallax = "#"+jQuery(this).attr('id');

                jQuery(sectionParallax).parallax("50%", "0.3");


            });

        });
    });

    
});


