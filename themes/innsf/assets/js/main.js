

jQuery(document).ready(function ($) {


/* horz describes each horizontal "slide"

note: bxslider is not active on these except sideslider, just legacy notation. */

$('section ul.bxslider > li, section ul.bxslider-1 > li, section ul.bxslider-2 > li, section ul.bxslider-3 > li').each(function() {
        if ($(this).siblings().size() > 0) {
$(this).addClass('horz');
}
});

// makes first pager item active

$('section #bx-pager-2 div a:nth-child(1), section #bx-pager-1 div a:nth-child(1), section #bx-pager-3 div a:nth-child(1)').each(function() {
    $(this).addClass('active');
});


// Add section class to sections

$('section').each(function() {
    if ($(this).is('.footer') || $(this).is('#top') || $(this).is('#sidebar') || $(this).is('.custom-header-option')){

    }
    else {
        var currentID = $(this).attr("id");
        $(this).addClass('section');
        $(this).attr("id",currentID+"-section");
    }
})


// data-anchors for fullPage.js

$('#menu-item-122').attr('data-menuanchor','home');
$('#menu-item-26').attr('data-menuanchor','rooms');
$('#menu-item-555').attr('data-menuanchor','specials');
$('#menu-item-554').attr('data-menuanchor','gallery');
$('#menu-item-23').attr('data-menuanchor','attractions');
$('#menu-item-25').attr('data-menuanchor','contact');




// Window-sizer based on resolution
    fullpager = $.fn.fullpage({
        navigation: true,
        css3:true,
        paddingTop: '48px',
        paddingBottom: '0',
        fixedElements: '#sidebar, #top, #ui-datepicker-div, #mc_embed_signup',
        navigationPosition: 'right',
        scrollingSpeed: 700,
        continuousVertical: true,
        normalScrollElements: '.gm-style, .gm-style div, .wpgmp_dropdown_ver_menu',
        anchors: ['home', 'rooms', 'specials', 'gallery', 'attractions', 'contact'],
        menu: '#main-menu',
        scrollOverflow: true,

        afterLoad: function(anchorLink, index){
            name = $('#main-menu li.active').children().html();
            ga('send', 'event', name, 'go to', 'nav');
            sideslider.goToSlide(index-1); 
        },
        afterhorzLoad: function(anchorLink, index, horzIndex, direction) {
            if(index=2){
        $('.bottom-navigation h3').removeClass('active');
        $('.bottom-navigation a img').removeClass('active');
        $('.bottom-navigation h3:nth-child('+(horzIndex+1)+')').addClass('active');
        $('.bottom-navigation a:nth-child('+(horzIndex+2)+') img').addClass('active');
            
        }
        if(index=3){
        $('#bx-pager-3 a').removeClass('active');
        $('#bx-pager-3 a:nth-child('+(horzIndex+1)+')').addClass('active');
        }
        if(index=5){
        $('#bx-pager-1 a').removeClass('active');
        $('#bx-pager-1 a:nth-child('+(horzIndex+1)+')').addClass('active');
        }
    }

    });

$('#load_screen').appendTo('body');


// Newsletter

$('#menu-item-324').click(function(e) {
e.preventDefault();
$(this).toggleClass('active');
$('#mc_embed_signup').slideToggle();
})

$('a[href*=#newsletter]').click(function(e) {
e.preventDefault();

$('#menu-item-324').toggleClass('active');
$('#mc_embed_signup').slideToggle();
})

// Rooms

$('#rooms-section li .heading h3').each( function() {
$(this).clone().appendTo('.room-name');
});

// Specials

$('#specials-section li .heading h3').each( function() {
$(this).clone().appendTo('.special-name');
});

$('#rooms-section .bottom-navigation img').each( function(i) {
    $(this).hover( function() {
        $('#rooms-section .bottom-navigation h3').hide();
        $('#rooms-section .bottom-navigation h3:nth-child('+(i+1)+')').toggle();
    }, function() {
    })
})

$('#specials-section .bottom-navigation img').each( function(i) {
    $(this).hover( function() {
        $('#specials-section .bottom-navigation h3').hide();
        $('#specials-section .bottom-navigation h3:nth-child('+(i+1)+')').toggle();
    }, function() {
    })
})

// Attractions

$(".page-sections-10 .location:not(:nth-child(5))").hover( function() {
    $(this).find('p').fadeToggle();
}, function() {
    $(this).find('p').fadeToggle();
    })

   

// Booking Calendar

 $( "#calender" ).datepicker({
minDate: 0,
defaultDate: null,
dateFormat: "mm/dd/yy",
onClose: function( selectedDate ) {
$( "#calender2" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#calender2" ).datepicker({
minDate: "1d",
defaultDate: "+1d",
dateFormat: "mm/dd/yy",
onClose: function( selectedDate ) {
$( "#calender" ).datepicker( "option", "maxDate", selectedDate );
}
});

// /*append extra content to menuication */

// enquire.register("screen and (max-width: 767px)", {
//     match : function() {
//         $( "#wp_menufication-multiple-content" ).append( $( "#extra-contents" ) );
//     },  
//     unmatch : function() {
//         $( "#extra-content" ).append( $( "#extra-contents" ) );
//     }
// });



/* hotel picker */

var properties = [ "Motel Capri", "Geary Parkway Motel", "Berkeley Rodeway Inn", "Quality Inn King City", "Marinwood Inn & Suites" ];

var property_sites = ["http://www.sfmotelcapri.com/","http://gearyparkwaymotel.com/","http://berkeleyri.com/","http://www.qualityinnkingcity.com/", "http://innnovato.com"];

// var property_avail = ["http://www.sfmotelcapri.com/index.php/reservations","http://www.gearyparkwaymotel.com/index.php/reservations","http://www.rodewayinn.com/hotel-berkeley-california-CA996","https://secure.reservation-booking-system.com/rez/indexweb.htm#lang=en-us&check_in=&check_out=&numberofrooms=&hotelid=15217&domain=www.qualityinnkingcity.com&PromotionalCode=&lastUrl=d3d3LnF1YWxpdHlpbm5raW5nY2l0eS5jb20lMmY=&roomsegment=&promotionid=&pid=8d3db77c67e67c00"];

$.each(properties, function( index, value ) {
  $('.picker_properties').append('<a class="picker_site" target="_blank" href="'+property_sites[index]+'">'+properties[index]+'</a>');
});

$('.picker_properties').hide();

$('.picker_button').click(function() {
    $('.picker_properties').slideToggle();
    $('.g_arrow').toggleClass('active');
})

// Newsletter




if ((window.location.hash=="") || (window.location.hash=="#home")) {
$('#menu-item-122').addClass('active');
}


// $('#bx-pager-3 a').click(function() {
//     $('#bx-pager-3 a').removeClass('active');
//     item = ($(this).index());
//     $.fn.fullpage.moveTo(3,item);
//     $(this).addClass('active');
// })

// $('#bx-pager-2 a').click(function() {
//     $('#bx-pager-2 a').removeClass('active');
//     item = ($(this).index());
//     $.fn.fullpage.moveTo(2,item);
//     $(this).addClass('active');
// })


// $('#bx-pager-1 a').click(function() {
//     $('#bx-pager-1 a').removeClass('active');
//     item = ($(this).index());
//     $.fn.fullpage.moveTo(5,item);
//     $(this).addClass('active');
// })


// Add social media to menufication
/*
$('#menufication-nav ul').append('<div id="sm-buttons"><a class="gp-menu" href="#"></a><a class="fb-menu" href="https://www.facebook.com/pages/Inn-San-Francisco/1457904334422518"></a></div>');*/

// Close menufication on menu click


$('#menufication-nav li a').click(function(e){
MENUFICATION_INSTANCE.menufication('closeMenu');
});



   

// Side Slider

    sideslider = $('.bxslider-sidebar').bxSlider({
        mode:'fade',
        controls: false,
        slideMargin: 30,
        pager: false,
        adaptiveHeight: true
    });

// mouse icon effect

    $('.image-wrapper').hover(function(){

        $(this).find('.mouse-effect').stop().animate({'opacity':'0.6'});
        $(this).find('.extra-links').stop().animate({'top':'50%'});

    },function(){


        $(this).find('.mouse-effect').stop().animate({'opacity':'0'});
        $(this).find('.extra-links').stop().animate({'top':'-50%'});

    });



// prettyPhoto and image borders

    $("a[rel^='prettyPhoto']").prettyPhoto();

    $(".ads img").addClass("img-frame");



   

$( "#cities-section ul li ul li" ).each(function() {
    var itemindex = $(this).index();
    var name = $(this).html();

    if ($(this).is(':has(span)')){
    }
    else {
  $( this ).wrapInner( "<span data-wiki='"+name+"'></span>" );
    }
});





//Analytics


$('#constant-contact-signup .button.submit').on('click', function() {
  ga('send', 'event', 'newsletter', 'submit', 'function');
});


$('#main-menu li a').on('click', function() {
    name = $(this).html();
  ga('send', 'event', name, 'click', 'nav');
});

$('#menufication-nav li a').on('click', function() {
    name = $(this).html();
  ga('send', 'event', name, 'click', 'mobile nav');
});

/*STANDARD ROOM*/



/*KITCHENETTE ROOM*/

// $("#rooms-section .container .horzsContainer > li:nth-child(2)").backstretch([
//     "http://innnovato.com/wp-content/uploads/2014/04/NovatoQI-20.jpg",
//     "http://innnovato.com/wp-content/uploads/2014/04/NovatoQI-12.jpg",
//     "http://innnovato.com/wp-content/uploads/2014/04/NovatoQI-13.jpg",    
//   ], {duration: 4000, fade: "normal"});

$('#rooms-section .container .horzsContainer > li').each(function() {
    $(this).on('click', function() {
$(this).backstretch("next");
});
});



$('body').prepend($('#menufication-outer-wrap'));
$('#menufication-outer-wrap #menufication-inner-wrap').append($('#superContainer'));
$('#superContainer').prepend($('#menufication-page-holder'));

$(window).load(function() {
$('#menufication-nav').append('<div class="site-info">© 2014 Marinwood Inn &amp; Suites<br>Website Design, Optimization and Hosting by<br><a href="http://www.optirev.net">Optirev, LLC</a> | <a href="/privacypolicy">Privacy Policy</a></div>');
$('div#load_screen').fadeOut();
});

$('.tableCell .container > .eight.columns.alpha').each(function(index) {
    if (index != 1){
  $(this).parent().prepend('<a class="eight columns hidebox" href="javascript:void(0)"><span>Hide</span><span>Show</span></a>');
}
});

$('.container > ul > li > div.eight.columns.alpha').each(function(index) {
  $(this).parent().prepend('<a class="eight columns hidebox" href="javascript:void(0)"><span>Hide</span><span>Show</span></a>');
});

$('.tableCell > .eight.columns.alpha').each(function(index) {
  $(this).parent().prepend('<a class="eight columns hidebox" href="javascript:void(0)"><span>Hide</span><span>Show</span></a>');
});


$('.hidebox').click(function() {
    $(this).toggleClass('active');
    $(this).children('span').toggle();
   $(this).siblings().fadeToggle();
    $('.bottom-navigation').fadeToggle();
})


    
});

jQuery(document).on('menufication-done', function() {
   jQuery('#menufication-nav a').on('click', function() {
jQuery('#my-menu').menufication('closeMenu');
document.body.scrollTop = document.documentElement.scrollTop = 0;

    })

  })

