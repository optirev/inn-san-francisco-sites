<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title('&raquo;', true, 'right' );?></title>

	<!-- Mobile Specific Metas
  ================================================== -->

  	<?php global $smof_data; ?>

	<?php if(isset($smof_data['nzs_responsive_layout']) && 0 == $smof_data['nzs_responsive_layout']): ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">

	<?php endif; ?>

		<!-- CSS
  ================================================== -->


	<?php

	 	if(!empty($smof_data['nzs_custom_favicon'])):
	 	
	 		echo '<link rel="icon" href="'.$smof_data['nzs_custom_favicon'].'">';

	 	endif;
  	?>
  	

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->



	<?php wp_head(); ?>

<style>html {
margin-top: 0 !important;
}</style>

<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '1525485484339955']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1525485484339955&amp;ev=NoScript" /></noscript>


</head>

<body <?php body_class(); ?>>
<div id="load_screen"><div id="loading"><img src="http://novato.optirev.net/wp-content/themes/innsf/images/loading.gif" /></div>
</div>

<div id="fb-root"></div>
<!--
<script type="text/javascript">
setTimeout(function() {
  if (location.hash) {
    window.scrollTo(0, 0);
  }
}, 1);
</script>


<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1428066920743162";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>-->
<!-- Primary Page Layout
	================================================== -->

<div id="mc_embed_signup">
<form action="//optirev.us10.list-manage.com/subscribe/post?u=48333ba66f6c6d1f8f525737a&amp;id=0864a46c09" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Subscribe to our mailing list</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_48333ba66f6c6d1f8f525737a_0864a46c09" tabindex="-1" value=""></div>
    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
    </div>
</form>
</div>


<!-- TOP BAR -->

<section class="topBar <?php echo nzs_menu_class();?>" id="top">
	<div class="container">
	


		<?php 

			if(isset($smof_data['nzs_plain_text_logo']) && 0 == $smof_data['nzs_plain_text_logo']){
				$use_logo = " class=\"hide-text\"";
			}
		 ?>

		<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<h1<?php echo (isset($use_logo)) ? $use_logo : ''?>><?php echo bloginfo('name');?></h1>
		</a>

		<nav class="mainMenu">
				

		
		<div id="sm-contact">
		
		<div id="header-contact">1-707-555-5555</div>
	
	
	<div style="position:absolute; margin-top:10px; left:-50px;">
	
<!-- 	<a href="https://plus.google.com/107784778932020460299/posts" rel="publisher"></a>
 -->	
<!-- 	<div class="g-plusone" data-annotation="inline" data-width="100"></div>
<script type="text/javascript">
      window.___gcfg = {
        lang: 'en-US'
      };

      (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
      })();
    </script> -->

</div>

	<span class="fb"></span>
	<span class="gp" ></span>
	<span class="ta" ></span>
	<a class="fb or" href="https://www.facebook.com/pages/Marinwood-Inn-Suites/121452817868273"></a>
	<a class="gp or" href="https://plus.google.com/107784778932020460299/posts"></a>
	<a class="ta or" href="http://www.tripadvisor.com/Hotel_Review-g32797-d661098-Reviews-Marinwood_Inn_Suites-Novato_Marin_County_California.html"></a>
</div>
			<?php wp_nav_menu( array( 'container' => '','menu_id'=> 'main-menu', 'theme_location' => 'primary','fallback_cb'=> '') );?>
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Extra Widgets')) :
endif; ?>
		</nav>


<!--End mc_embed_signup-->	</div> 


<!-- ./container -->
</section> <!-- ./topBar -->
<!-- Begin MailChimp Signup Form -->
<!-- <link href="//cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css">
 -->

<section id="sidebar">
<div id="logo-wrapper">
<a href="/" id="sidebar-logo">
</a>
</div>

<ul class="bxslider-sidebar">
  <li><div><h3>Home</h3><div id="navarrows"></div>You can navigate with your keyboard.</li>
  <li><div><h3>Rooms</h3>From standard, kitchenette to extended-stay, we offer 47 newly-remodeled guest rooms. Each room is tastefully decorated, providing the ultimate in comfort and convenience.</div></li>
  <li><div><h3>Specials</h3>We have an assortment of packages to help you coordinate the perfect getaway. Save by calling and booking direct!</div></li>
  <li><div><h3>Gallery</h3>Get a feel for the room selections in our gallery of photos. Find the best fit for you.</div></li>
  <li><div><h3>Local</h3>We are only 40 miles north of the San Francisco Int'l Airport, centrally located in the shadow of Mt. Tamalpais, with close proximity to rugged coastline, renowned wineries, fine dining, stunning vistas, spas and other activity-rich destinations.</div></li>
  <li><div><h3>Contact</h3>Help us help you better. <br><br>We'd love to hear your comments and feedback</div></li>
</ul>



<!-- <form action="https://booking.ihotelier.com/istay/istay.jsp" method="get" name="iform" id="iform">
<input type="hidden" name="HotelID" value="96280" />
<input type="hidden" name="LanguageID" value="#" />
<input type="hidden" name="Rooms" value="1" /> 

<div class="checkdates">
Check-In Date: <input id="calender" class="datein" name="DateIn" type="text" />
</div>
<div class="checkdates">
Check-Out Date: <input id="calender2" class="datein" name="DateOut" type="text" />
</div>
<br>

<div class="floatleft">
Adults:
<select name="Adults">
<option value="1" selected="selected">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
</select>
</div>

<div class="floatleft">
Children:
<select name="Children">
<option value="0" selected="selected">0</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
</select>
</div>

<input class="booksubmit" type="submit" value="Submit" />
</form>
 -->

<script type="text/javascript" language="javascript">
function getDateStr() {
	var today = new Date();
	var todayStr = (today.getMonth()+1) + "/" + today.getDate() + "/" + today.getFullYear();;
	document.iform.DateIn.value = todayStr;
}
getDateStr();

</script>
<!--
<div id="sm-sidebar">
	<a class="fb" href="https://www.facebook.com/pages/Inn-San-Francisco/1457904334422518"></a>
	<a class="gp" href="#"></a>
</div>-->
<div id="extra-contents">
<div id="picker">
<div class="picker_button">OUR OTHER HOTELS <div class="g_arrow"></div></div>
<div class="picker_properties">
</div>
</div>
</div>



<section class="footer" id="foot">
	<div class="container">
	&copy; 2014-<?php echo date("Y") ?> Marinwood Inn &#38; Suites<br>Website design, optimization and hosting by <a href="http://optirev.net">OptiRev, LLC</a> | <a href="/privacypolicy">Privacy Policy</a>
	</div><!-- ./container -->
</section><!-- ./footer -->

</section>

<!-- ENDS TOP BAR -->

<?php
echo nzs_show_header();
?>
