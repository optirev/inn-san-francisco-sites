module.exports = function(grunt) {

'use strict';

  var config = {

    watch: {
     
      js: {
        files: 'library/js/*.js',
        options: { livereload: 35729 }
      },
      php: {
        files: '*.php',
        options: { livereload: 35729 }
      },
      livereload: {
        options: {
          livereload: true
        },
        files: ['**/*.css']
      }
    },


  };

  // Default task
  grunt.registerTask('default', ['watch']);

  // Load up tasks
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Register tasks
  grunt.initConfig(config);
};


