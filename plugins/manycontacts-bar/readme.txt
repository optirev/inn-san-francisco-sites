=== ManyContacts Bar - Free Contact Form ===
Contributors: woorank
Tags: manycontacts, manycontacts bar, bar, toolbar, notification bar
Requires at least: 2.9.0
Tested up to: 3.5.1
Stable tag: 2.1.4

A Free Top Bar for your website to get many contacts

== Description ==
[ManyContacts](http://www.manycontacts.com/) is a free contact form sits on top of your website to grab visitors’ attention. We make it easy for you to offer a giveaway - such as an ebook, coupon code, a test, a course - without creating extra landing pages.

Over 10,000 E-commerce stores, local businesses, service providers, freelancers and bloggers use ManyContacts everyday to increase their conversion rate.

Created by online marketing experts from Woorank

Last but not least, it's completely FREE. No strings attached.

### What's new? ###

*   Integration with MailChimp


== Installation ==
1. Upload the `manycontacts-bar` folder and all its contents to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Paste your manycontacts bar snippet from [manycontacts.com](http://www.manycontacts.com/) into the text area and click save!


== Frequently Asked Questions ==

### What is ManyContacts? ###
ManyContacts is an attention­-grabbing contact form that sits at the top of your website and helps to convert visitors into customers. 

### Who uses ManyContacts? ###
Everyone who wants to turn website visitors into customers. Typical users are: 
*   Ecommerce owners
*   Small businesses
*   Freelancers
*   Bloggers and 
*   Web-agencies.

### How do I add ManyContacts on my site? ###
First, create an account and start customizing your bar. When you are happy with your bar, we will give you a piece of code that you will need paste in your source code just before the </body> tag.


= Where do I get the ManyContacts Bar code for this plugin? =

You can get your own ManyContacts Bar account from [manycontacts.com](http://www.manycontacts.com/) for free.
Once you have your account, create a ManyContacts Bar and simply paste your code into the plugin's textarea.


== Screenshots ==
1. The ManyContacts Bar for WordPress interface.
2. How it looks on your website.

== Changelog ==

= 2.1.4 =
* Header issue update.

= 2.1.2 =
* Info update.

= 2.0 =
* Info update.

= 1.0 =
* Initial release.
